import { Component } from '@angular/core';
import { SignalarService } from './services/signalar.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  message: string;
  constructor(public signalarService: SignalarService) {
    this.signalarService.initialize();
  }
  title = 'signalr-poc';
}
