import { TestBed, inject } from '@angular/core/testing';

import { SignalarService } from './signalar.service';

describe('SignalarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignalarService]
    });
  });

  it('should be created', inject([SignalarService], (service: SignalarService) => {
    expect(service).toBeTruthy();
  }));
});
