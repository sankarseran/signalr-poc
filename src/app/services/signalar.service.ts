import { Injectable } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';

@Injectable({
  providedIn: 'root'
})
export class SignalarService {
  private connection: HubConnection | undefined;
  public async: any;
  message = '';
  messages: string[] = [];

  constructor() { }

  initialize() {
    // tslint:disable-next-line:max-line-length
    const t = JSON.parse(localStorage.getItem('token'));
    // tslint:disable-next-line:max-line-length
    const token = encodeURIComponent('5m3oK8AHskzZxlwqgLzVt7AYTHFTU2MNybcIC0PEEFki7AlpjT8BA2QHfswofvGHicksq74shGVXooqqKlB8Xp5y33H2bgfFL7armiTbNLX35JWsBSTklhaAmveo4rPlv/zA/CzJrtPvhSo9VxfQ5pSb6q9PJwQCf0sWpHkIugiedJW0tTT9WbmVLQnOh0zwtQVIG9bvLm83BhSJpZByRzmmDipXWY7+pV0SG2zN2LuY0Wsgbzt4l160NV8hLqwqDqWqDm0GH6+lgRSFYK25R57Q/Q7Mtk4z2JQkAJUymcBiUU/DMzHSIA5UUuC4+fAn');
    console.log(token, t);
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl('https://ubiquifit-dev.augustasoftsol.com:5110/chathub?' +
        'sessionId=' + 686 +
        '&sessionSubscriptionId=' + 1779 +
        '&token=' + token)
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this.connection.on('newMessageReceived', (data: any) => {
      console.log('data', data);
      const received = `Received: ${data}`;
      this.messages.push(data.message);
    });

    this.connection.on('updateViewersCount', (data: any) => {
      console.log('data', data);
      const received = `Received: ${data}`;
      this.messages.push(data.message);
    });

    this.connection.start().catch(err => console.error(err.toString()));
  }

  public sendMessage(message): void {
    const data = `Sent: ${message}`;

    if (this.connection) {
      this.connection.invoke('SendMessage', ...message).then((result) => {
        console.log('invoked result:', result);
        this.messages.push(data);
      }).catch((err) => {
        console.log(err);
      });
    }
  }

  disconnet() {
    return this.connection.stop();
  }
}
